<?php

namespace App\Libraries;

use App\Libraries\WpUser;
use Illuminate\Support\Facades\Log;
use App\Libraries\WpUserMetaSessionTokens;

class WpCookies
{
    
    
                public $cookie_hash;
                
                //LOGGED_IN_COOKIE
                public $logged_in_cookie_name;
                
                public $cookie_domain;
    
                public function __construct($siteurl='') { 
                    
                    if(empty($siteurl)) {
                        $siteurl = env("WP_SITEURL");
                    }
                    $this->cookie_hash = env('WP_COOKIE_HASH', \md5( $siteurl ));
                    
                    $this->logged_in_cookie_name = 'wordpress_logged_in_' . $this->cookie_hash;
                    
                    $this->cookie_domain = env("WP_COOKIE_DOMAIN");
                    
                }
                
                /**
                 * Sets the authentication cookies based on user ID.
                 *
                 * The $remember parameter increases the time that the cookie will be kept. The
                 * default the cookie is kept without remembering is two days. When $remember is
                 * set, the cookies will be kept for 14 days or two weeks.
                 *
                 * @since 2.5.0
                 *
                 * @param int   $user_id User ID
                 * @param bool  $remember Whether to remember the user
                 * @param mixed $secure  Whether the admin cookies should only be sent over HTTPS.
                 *                       Default is_ssl().
                 */             
                public function wp_set_auth_cookie( $user_id, $remember = false, $secure = '' ) {
                    
                    /**
                     * Filter the duration of the authentication cookie expiration period.
                     *
                     * @since 2.8.0
                     *
                     * @param int  $length   Duration of the expiration period in seconds.
                     * @param int  $user_id  User ID.
                     * @param bool $remember Whether to remember the user login. Default false.
                     */
                    $expiration = time() + (14 * 86400);

                    /*
                     * Ensure the browser will continue to send the cookie after the expiration time is reached.
                     * Needed for the login grace period in wp_validate_auth_cookie().
                     */
                    $expire = $expiration + ( 12 * 60 );

                    if ( '' === $secure ) {
                            $secure =  $this->is_ssl();
                    }

                    // Frontend cookie is secure when the auth cookie is secure and the site's home URL is forced HTTPS.
                    $secure_logged_in_cookie = $secure;

                    $token_manager           = new WpUserMetaSessionTokens($user_id);
                    
                    $token = $token_manager->create( $expiration );                   

                    $logged_in_cookie_value = $this->wp_generate_auth_cookie( $user_id, $expiration, 'logged_in', $token );
                    
                    $logged_in_cookie_array = array(
                                'name'     => $this->logged_in_cookie_name,
                                'value'    => $logged_in_cookie_value,
                                'expire'   => $expire,
                                'path'     => '/',
                                'domain'   => $this->cookie_domain,
                                'secure'   => $secure_logged_in_cookie,
                                'httponly' => true
                    );
                    
                    return $logged_in_cookie_array;
                    
                }
                
                
                /**
                 * Generate authentication cookie contents.
                 *
                 * @since 2.5.0
                 *
                 * @param int $user_id User ID
                 * @param int $expiration Cookie expiration in seconds
                 * @param string $scheme Optional. The cookie scheme to use: auth, secure_auth, or logged_in
                 * @param string $token User's session token to use for this cookie
                 * @return string Authentication cookie contents. Empty string if user does not exist.
                 */
                public function wp_generate_auth_cookie( $user_id, $expiration, $scheme = 'logged_in', $token = '' ) {
                    
                        $user = WpUser::instance('id', $user_id);
                        
                        if ( empty( $user->data ) ) {
                                return false;
                        }

                        $pass_frag  = substr($user->user_pass, 8, 4);
                        $key        = $this->wp_hash( $user->user_login . '|' . $pass_frag . '|' . $expiration . '|' . $token, $scheme );
                        $hash       = hash_hmac( 'sha256', $user->user_login . '|' . $expiration . '|' . $token, $key ); 
                        $cookie     = $user->user_login . '|' . $expiration . '|' . $token . '|' . $hash;

                        return $cookie;
                } 
                
                /**
                 * Validates authentication cookie.
                 *
                 * The checks include making sure that the authentication cookie is set and
                 * pulling in the contents (if $cookie is not used).
                 *
                 * Makes sure the cookie is not expired. Verifies the hash in cookie is what is
                 * should be and compares the two.  
                 *
                 * @since 2.5.0
                 *
                 * @param string $cookie Optional. If used, will validate contents instead of cookie's
                 * @param string $scheme Optional. The cookie scheme to use: auth, secure_auth, or logged_in
                 * @return bool|obj False if invalid cookie, WpUser obj if valid.
                 */
                public function wp_validate_auth_cookie( $cookie = '', $scheme = 'logged_in' ) {
                        if ( ! $cookie_elements = $this->wp_parse_auth_cookie($cookie) ) {
                                /**
                                 * If an authentication cookie is malformed.
                                 */
                                return false;
                        }

                        $username   = $cookie_elements['username'];
                        $hmac       = $cookie_elements['hmac'];
                        $token      = $cookie_elements['token'];
                        $expired    = $expiration = $cookie_elements['expiration'];


                        // Quick check to see if an honest cookie has expired
                        if ( $expired < time() ) {
                            /**
                             *  If authentication cookie has expired.
                             */
                            return false;
                        } 

                        if ( ! $user = new WpUser('login', $username) ) {
                                return false;
                        }

                        $pass_frag = substr($user->user_pass, 8, 4);

                        $key = $this->wp_hash( $username . '|' . $pass_frag . '|' . $expiration . '|' . $token, $scheme );

                        $hash = hash_hmac( 'sha256', $username . '|' . $expiration . '|' . $token, $key );       

                        //user NOT logged into WP
                        if ( ! hash_equals( $hash, $hmac ) ) {
                                /**
                                 * Fires if a bad authentication cookie hash is encountered.
                                 */
                                return false;
                        }
 
                        return $user;
                }                

 
                
                /**
                 * Determine if SSL is used.
                 *
                 * @since 2.6.0
                 *
                 * @return bool True if SSL, false if not used.
                 */
                public function is_ssl() {
                        if ( isset($_SERVER['HTTPS']) ) {
                                if ( 'on' == strtolower($_SERVER['HTTPS']) )
                                        return true;
                                if ( '1' == $_SERVER['HTTPS'] )
                                        return true;
                        } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
                                return true;
                        }
                        return false;
                }                
                
                    /**
                     * Parse a cookie into its components
                     *
                     * @since 2.7.0
                     *
                     * @param string $cookie
                     * @param string $scheme Optional. The cookie scheme to use: auth, secure_auth, or logged_in
                     * @return array Authentication cookie components:
                     * array:4 [
                        "username"      => "admin"
                        "expiration"    => "1534615760"
                        "token"         => "lgn9OmUsB4WQKNEE2BHmUwdx6FfppBoNMOedFsIFosN"
                        "hmac"          => "2ddefa66cbcbf82ff0b82fd61fbcb38520cd0950b4847528a2823100c1287698"
                        ]
                     */
                    public function wp_parse_auth_cookie() {
                        
                        $cookie = isset($_COOKIE['wordpress_logged_in_' . $this->cookie_hash]) ? $_COOKIE['wordpress_logged_in_' . $this->cookie_hash] : '';

                        $cookie_elements = explode('|', $cookie);
                        if ( count( $cookie_elements ) !== 4 ) {
                                return false;
                        }

                        list( $username, $expiration, $token, $hmac ) = $cookie_elements;

                        return compact( 'username', 'expiration', 'token', 'hmac', 'scheme' );
                }                
                



                        
                        
                        /**
                         * Get hash of given string.
                         *
                         * @since 2.0.3
                         *
                         * @param string $data   Plain text to hash
                         * @param string $scheme Authentication scheme (auth, secure_auth, logged_in, nonce)
                         * @return string Hash of $data
                         */
                        public function wp_hash($data, $scheme = 'auth') {
                                $salt = $this->wp_salt($scheme);

                                return hash_hmac('md5', $data, $salt);
                        } 
                        
                        /**
                         * Get salt to add to hashes.
                         *
                         * Salts are created using secret keys. Secret keys are located in two places:
                         * in the database and in the wp-config.php file. The secret key in the database
                         * is randomly generated and will be appended to the secret keys in wp-config.php.
                         *
                         * The secret keys in wp-config.php should be updated to strong, random keys to maximize
                         * security. Below is an example of how the secret key constants are defined.
                         * Do not paste this example directly into wp-config.php. Instead, have a
                         * {@link https://api.wordpress.org/secret-key/1.1/salt/ secret key created} just
                         * for you.
                         *
                         *     define('AUTH_KEY',         ' Xakm<o xQy rw4EMsLKM-?!T+,PFF})H4lzcW57AF0U@N@< >M%G4Yt>f`z]MON');
                         *     define('SECURE_AUTH_KEY',  'LzJ}op]mr|6+![P}Ak:uNdJCJZd>(Hx.-Mh#Tz)pCIU#uGEnfFz|f ;;eU%/U^O~');
                         *     define('LOGGED_IN_KEY',    '|i|Ux`9<p-h$aFf(qnT:sDO:D1P^wZ$$/Ra@miTJi9G;ddp_<q}6H1)o|a +&JCM');
                         *     define('NONCE_KEY',        '%:R{[P|,s.KuMltH5}cI;/k<Gx~j!f0I)m_sIyu+&NJZ)-iO>z7X>QYR0Z_XnZ@|');
                         *     define('AUTH_SALT',        'eZyT)-Naw]F8CwA*VaW#q*|.)g@o}||wf~@C-YSt}(dh_r6EbI#A,y|nU2{B#JBW');
                         *     define('SECURE_AUTH_SALT', '!=oLUTXh,QW=H `}`L|9/^4-3 STz},T(w}W<I`.JjPi)<Bmf1v,HpGe}T1:Xt7n');
                         *     define('LOGGED_IN_SALT',   '+XSqHc;@Q*K_b|Z?NC[3H!!EONbh.n<+=uKR:>*c(u`g~EJBf#8u#R{mUEZrozmm');
                         *     define('NONCE_SALT',       'h`GXHhD>SLWVfg1(1(N{;.V!MoE(SfbA_ksP@&`+AycHcAV$+?@3q+rxV{%^VyKT');
                         *
                         * Salting passwords helps against tools which has stored hashed values of
                         * common dictionary strings. The added values makes it harder to crack.
                         *
                         * @since 2.5.0
                         *
                         * @link https://api.wordpress.org/secret-key/1.1/salt/ Create secrets for wp-config.php
                         *
                         * @staticvar array $cached_salts
                         * @staticvar array $duplicated_keys
                         *
                         * @param string $scheme Authentication scheme (auth, secure_auth, logged_in, nonce)
                         * @return string Salt value
                         */
                        public function wp_salt( $scheme = 'auth' ) {
                                static $cached_salts = array();

                                $values             =  array();
                                $values['key']      =  env("WP_LOGGED_IN_KEY");
                                $values['salt']     =  env("WP_LOGGED_IN_SALT");

                                $cached_salts[ $scheme ] = $values['key'] . $values['salt'];

                                return $cached_salts[ $scheme ];
                        }                        
                        
                        
                
}




    
