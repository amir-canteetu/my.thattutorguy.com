<?php

namespace App\Libraries;

use Illuminate\Support\Facades\DB;


class WpUser
{
	/**
	 * User data container.
	 *
	 * @since 2.0.0
	 * @var object
	 */
	public $data;

	/**
	 * The user's ID.
	 *
	 * @since 2.1.0
	 * @var int
	 */
	public $ID = null;
        
	protected static $_instance = null; 
        
        private $dbservername;
        
        private $dbusername;
        
        private $dbpassword;
        
        private $dbname;

	/**
	 * Constructor.
	 *
	 * Retrieves the userdata and passes it to WP_User::init().
	 *
	 * @since 2.0.0
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param int|string|stdClass|WP_User $id User's ID, a WP_User object, or a user object from the DB.
	 * @param string $name Optional. User's username
	 * @param int $site_id Optional Site ID, defaults to current site.
	 */
	public function __construct( $field, $value ) {
            
                $this->dbservername     = env("DB_HOST_WORDPRESS");
                $this->dbusername       = env("DB_USERNAME_WORDPRESS");
                $this->dbpassword       = env("DB_PASSWORD_WORDPRESS");
                $this->dbname           = env("DB_DATABASE_WORDPRESS");            
            
		// 'ID' is an alias of 'id'.
		if ( 'ID' === $field ) {
			$field = 'id';
		}

		if ( 'id' == $field ) {
			if ( ! is_numeric( $value ) )
				return false;
			$value = intval( $value );
			if ( $value < 1 )
				return false;
		} else {
			$value = trim( $value );
		}

		if ( !$value )
			return false;

		switch ( $field ) {
			case 'id':
				$user_id = $value;
				$db_field = 'ID';
				break;
			case 'email':
				$db_field = 'user_email';
				break;
			case 'login':
				$db_field = 'user_login';
				break;
			default:
				return false;
		}

		if ( !$userdata  =   $this->get_wp_user_by( $db_field, $value ) ) {
                    return false;
                }
		$this->data         = $userdata;
		$this->ID           = (int) $userdata['ID'];

	}
        
	public static function instance( $field, $value ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $field, $value );
		}
		return self::$_instance;
	}

        public function get_user_meta( $user_id, $meta_key ){

                    try 
                    {
                    $pdo            = new \PDO("mysql:host=$this->dbservername;dbname=$this->dbname", $this->dbusername, $this->dbpassword);
                    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                    $stmt           = $pdo->prepare('SELECT ? FROM wp_usermeta WHERE user_id = ?');
                    $stmt->execute( [ $meta_key, $user_id ] );
                    $wp_user_meta   = $stmt->fetch();
                    }
                    catch(PDOException $e)
                    {
                    echo "Connection failed: " . $e->getMessage();
                    }                    
                    
                    if( is_array( $wp_user_meta ) && !( empty( $wp_user_meta )) ):
                        
                        return   $wp_user_meta;  
                    
                    else:
                        
                        return   false;  
                        
                    endif;            
            
        }

        /**
	 * Return only the main user fields
	 *
	 * @since 3.3.0
	 * @since 4.4.0 Added 'ID' as an alias of 'id' for the `$field` parameter.
	 *
	 * @static
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param string $field The field to query against: 'id', 'ID', 'slug', 'email' or 'login'.
	 * @param string|int $value The field value
	 * @return object|false Raw user object
	 */
	public function get_wp_user_by( $db_field, $value ) {

                try 
                {
                $pdo            = new \PDO("mysql:host=$this->dbservername;dbname=$this->dbname", $this->dbusername, $this->dbpassword);
                $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                $stmt           = $pdo->prepare( "SELECT * FROM wp_users WHERE $db_field = ?" );
                $stmt->execute( [trim( $value )] );
                $userdata       = $stmt->fetch();
                }
                catch(PDOException $e)
                {
                echo "Connection failed: " . $e->getMessage();
                }                    

                if( !is_array( $userdata ) || ( empty( $userdata ) ) ):

                    return   false;                     

                endif;            

                return $userdata;
	}
        
        
	public function update_user_meta( $user_id, $meta_key, $meta_value ) {

                try 
                {
                $pdo            = new \PDO("mysql:host=$this->dbservername;dbname=$this->dbname", $this->dbusername, $this->dbpassword);
                $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                $stmt           = $pdo->prepare( "INSERT INTO wp_usermeta (user_id, meta_key, meta_value) VALUES (?,?,?)" );
                $affected_rows  = $stmt->execute( [ $user_id, $meta_key, $meta_value ] );
                
                }
                catch(PDOException $e)
                {
                echo "Connection failed: " . $e->getMessage();
                } 

                if( !$affected_rows ):

                    return   false;
                
                else:
                    
                    return   true;

                endif;            

	}         
        
        
	/**
	 * Magic method for accessing custom fields.
	 *
	 * @since 3.3.0
	 *
	 * @param string $key User meta key to retrieve.
	 * @return mixed Value of the given user meta key (if set). If `$key` is 'id', the user ID.
	 */
	public function __get( $key ) {

		if ( isset( $this->data[$key] ) ) {
			$value = $this->data[$key];
                        return $value;
		} 

		
	}  
        

        /**
	 * Get wordpress user object
	 *
	 * @param @param  Request $request
	 * @return object|false wp user object
	 */        
        public static function checkForWpUser( \Illuminate\Http\Request $request )
        {

            $wp_users        = DB::connection('wordpress')->select('SELECT * FROM wp_users WHERE user_email = ?', [ trim( $request->email ) ]);

            if( is_array( $wp_users ) && !( empty( $wp_users )) ):

                if( is_object($wp_users[0]) ):
                    $wp_user_obj        = $wp_users[0];                    
                    return   $wp_user_obj;     
                endif;

            endif;

            return false;

        }        
        
                
              
                
}




    