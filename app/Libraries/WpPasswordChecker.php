<?php

namespace App\Libraries;


use App\Libraries\PasswordHash;


class WpPasswordChecker {
    
    
                public function __construct() {
                    return $this;
                }
           
    
                /**
                 * Checks the plaintext password against the encrypted Password.
                 *
                 * Maintains compatibility between old version and the new cookie authentication
                 * protocol using PHPass library. The $hash parameter is the encrypted password
                 * and the function compares the plain text password when encrypted similarly
                 * against the already encrypted password to see if they match.
                 *
                 * For integration with other applications, this function can be overwritten to
                 * instead use the other package password checking algorithm.
                 *
                 * @since 2.5.0
                 *
                 * @global object $wp_hasher PHPass object used for checking the password
                 *	against the $hash + $password
                 * @uses PasswordHash::CheckPassword
                 *
                 * @param string $password Plaintext user's password
                 * @param string $hash Hash of the user's password to check against.
                 * @return bool False, if the $password does not match the hashed password
                 */
                public static function wp_check_password($password, $hash, $user_id = '') { 
                    
                        // If the hash is still md5...
                        if ( strlen($hash) <= 32 ) {

                                return $this->hash_equals( $hash, md5( $password ) );
                        }                     
                    

                        $wp_hasher = new PasswordHash( 8, true );

                        return $wp_hasher->CheckPassword($password, $hash);

                }  
                
                
                /**
                 * Compare two strings in constant time.
                 *
                 * This function was added in PHP 5.6.
                 * It can leak the length of a string.
                 *
                 * @since 3.9.2
                 *
                 * @param string $a Expected string.
                 * @param string $b Actual string.
                 * @return bool Whether strings are equal.
                 */
                private function hash_equals( $a, $b ) {
                        $a_length = strlen( $a );
                        if ( $a_length !== strlen( $b ) ) {
                                return false;
                        }
                        $result = 0;

                        // Do not attempt to "optimize" this.
                        for ( $i = 0; $i < $a_length; $i++ ) {
                                $result |= ord( $a[ $i ] ) ^ ord( $b[ $i ] );
                        }

                        return $result === 0;
                }               

        
}