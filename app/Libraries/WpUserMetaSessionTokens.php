<?php

namespace App\Libraries;

use App\Libraries\WpUser;
use Illuminate\Support\Facades\Log;
use App\Libraries\PasswordHash;


/**
 * Meta-based user sessions token manager.
 *
 * @since 4.0.0
 */
class WpUserMetaSessionTokens {
    
    
	/**
	 * User ID.
	 *
	 * @since 4.0.0
	 * @access protected
	 * @var int User ID.
	 */
	protected $user_id;

	/**
	 * Protected constructor.
	 *
	 * @since 4.0.0
	 *
	 * @param int $user_id User whose session to manage.
	 */
	public function __construct( $user_id ) {
		$this->user_id = $user_id;
	}   
    
    
	/**
	 * Generate a session token and attach session information to it.
	 *
	 * A session token is a long, random string. It is used in a cookie
	 * link that cookie to an expiration time and to ensure the cookie
	 * becomes invalidated upon logout.
	 *
	 * This function generates a token and stores it with the associated
	 * expiration time (and potentially other session information via the
	 * `attach_session_information` filter).
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param int $expiration Session expiration timestamp.
	 * @return string Session token.
	 */
	final public function create( $expiration ) {
		/**
		 * Filter the information attached to the newly created session.
		 *
		 * Could be used in the future to attach information such as
		 * IP address or user agent to a session.
		 *
		 * @since 4.0.0
		 *
		 * @param array $session Array of extra data.
		 * @param int   $user_id User ID.
		 */
		$session                = array();
		$session['expiration']  = $expiration;

		// IP address.
		if ( !empty( $_SERVER['REMOTE_ADDR'] ) ) {
			$session['ip'] = $_SERVER['REMOTE_ADDR'];
		}

		// User-agent.
		if ( ! empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
			$session['ua'] = stripslashes( $_SERVER['HTTP_USER_AGENT'] );
		}

		// Timestamp
		$session['login']   = time();

		$password_hash      = new PasswordHash(8, true);
                $token              = $password_hash->wp_generate_password( 43, false, false );

		$this->update( $token, $session );

		return $token;
	} 


	/**
	 * Update a session token.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param string $token Session token to update.
	 * @param array  $session Session information.
	 */
	final public function update( $token, $session ) {
		$verifier = $this->hash_token( $token );
		$this->update_session( $verifier, $session );
	}

	/**
	 * Hashes a session token for storage.
	 *
	 * @since 4.0.0
	 * @access private
	 *
	 * @param string $token Session token to hash.
	 * @return string A hash of the session token (a verifier).
	 */
	final private function hash_token( $token ) {
		// If ext/hash is not present, use sha1() instead.
		if ( function_exists( 'hash' ) ) {
			return hash( 'sha256', $token );
		} else {
			return sha1( $token );
		}
	}        

	/**
	 * Update a session by its verifier.
	 *
	 * @since 4.0.0
	 * @access protected
	 *
	 * @param string $verifier Verifier of the session to update.
	 * @param array  $session  Optional. Session. Omitting this argument destroys the session.
	 */
	protected function update_session( $verifier, $session = null ) {
		$sessions = $this->get_sessions();

		if ( $session ) {
			$sessions[ $verifier ] = $session;
		} else {
			unset( $sessions[ $verifier ] );
		}

		$this->update_sessions( $sessions );
	}
        
	/**
	 * Get all sessions of a user.
	 *
	 * @since 4.0.0
	 * @access protected
	 *
	 * @return array Sessions of a user.
	 */
	protected function get_sessions() {
            
                $user       = WpUser::instance( 'id', $this->user_id );
		$sessions   = $user->get_user_meta( $this->user_id, 'session_tokens', true );

		if ( ! is_array( $sessions ) ) {
			return array();
		}

	}

	/**
	 * Converts an expiration to an array of session information.
	 *
	 * @param mixed $session Session or expiration.
	 * @return array Session.
	 */
	protected function prepare_session( $session ) {
		if ( is_int( $session ) ) {
			return array( 'expiration' => $session );
		}

		return $session;
	}

	/**
	 * Retrieve a session by its verifier (token hash).
	 *
	 * @since 4.0.0
	 * @access protected
	 *
	 * @param string $verifier Verifier of the session to retrieve.
	 * @return array|null The session, or null if it does not exist
	 */
	protected function get_session( $verifier ) {
		$sessions = $this->get_sessions();

		if ( isset( $sessions[ $verifier ] ) ) {
			return $sessions[ $verifier ];
		}

		return null;
	}


	/**
	 * Update a user's sessions in the usermeta table.
	 *
	 * @since 4.0.0
	 * @access protected
	 *
	 * @param array $sessions Sessions:
            array(1) {
              ["e342203944dee6e1e7395a6c46d6d205b539aa1ae1db7d4c7f4aa0196e84d05a"]=>
              array(4) {
                ["expiration"]=>
                int(1536413862)
                ["ip"]=>
                string(12) "197.99.80.41"
                ["ua"]=>
                string(102) "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36"
                ["login"]=>
                int(1535204262)
              }
            }
	 */
	protected function update_sessions( $sessions ) {
            
                $user       = WpUser::instance( 'id', $this->user_id );
                $sessions   = serialize($sessions);
		return $user->update_user_meta( $this->user_id, 'session_tokens', $sessions );
                
	}
        
        
        
        


}




    