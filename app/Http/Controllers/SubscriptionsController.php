<?php

namespace App\Http\Controllers;


use App\Libraries\WlmApiClass;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;
use App\Libraries\WpCookies;
use App\Models\Plan;
use App\Models\User;
use Carbon\Carbon;




class SubscriptionsController extends Controller
{

                private $level;
                
                private $plan;
                
                private $user_wp_id;
                
                private $user;
                
                private $has_wp_logged_in_cookie = false; 
                
                private $wp_cookies;
                
                private $wlmapi;
                
                private $trialLength;
                
                public function __construct( Request $request, WpCookies $wp_cookies, WlmApiClass $wlmapi ) 
                {
                    
                    if( $this->plan         = Plan::where('slug', $request->plan)->first()):
                        $this->level        =  $this->plan->braintree_plan;
                    endif;

                    $this->wlmapi           = $wlmapi;
                    
                    $this->wp_cookies       = $wp_cookies;
                    $cookie                 = $this->wp_cookies->wp_parse_auth_cookie(); 
                    
                    if( $this->wp_cookies->wp_validate_auth_cookie( $cookie ) ):
                        $this->has_wp_logged_in_cookie = true;
                    endif;                      
                    
                }

                /**
                 * Subscribes user.
                 *
                 * @param  Request $request
                 * @return void
                 */
                public function store( Request $request )
                {

                        $this->validate($request, [
                            'email'     => 'bail|required|email',
                            'password'  => 'required|min:6',
                            'name'      => 'required'
                        ]); 

                        if( $user = UsersController::getUser( $request, $this->has_wp_logged_in_cookie ) ):
                            
                            $this->user = $user;
                             
                            if(isset($user->wp_id)):
                                $this->user_wp_id   = $user->wp_id;  
                            endif;
                            
                        else:
                            return redirect()->back()->withInput()->with( ['email' => $request->email] );
                        endif;
                        
                        if( $this->user->subscribedToPlan( $this->plan->braintree_plan, $request->plan ) ):   

                            $request->session()->flash( 'message.level', 'info' );
                            $request->session()->flash( 'message.content', 'You\'re already subscribed to this plan.');   
                            return redirect()->back();

                        endif;  

                        if( empty( $this->plan ) ):

                            $request->session()->flash( 'message.level', 'warning' );
                            $request->session()->flash( 'message.content', 'Sorry; the plan you tried to subscribe to does not exist.'  );                              
                            return redirect()->back();

                        endif;

                        $this->subscribeUser($request);

                        $this->user = User::find($this->user->id);

                        if ( $this->user->subscribed( $request->plan ) ):

                            $expiry     = Carbon::now()->addDays( $this->trialLength );
                            $timestamp  = $expiry->timestamp;

                            if( empty( $this->user->wp_id ) ):

                                $json_array = $this->wlmapi->createWishListMember( $request, $timestamp, $this->level  );

                                if( 1 == $json_array['success']):

                                    $this->user_wp_id   = $this->user->wp_id = $json_array['member']["id"];
                                    $this->user->save();  

                                endif;

                            else:

                               $json_array = $this->wlmapi->updateWishListMembership( $request, $timestamp, $this->level, $this->user_wp_id  ); 

                            endif;

                            if( 0 == $json_array['success'] ):

                                $request->session()->flash( 'message.level', 'warning' );
                                $request->session()->flash( 'message.content', 'We could not subscribe you to this plan; please try again or contact thattutorguy for assistance.' );                                         
                                return redirect()->back();

                            endif;


                            if( !$this->has_wp_logged_in_cookie ):

                                $cookie          = $this->wp_cookies->wp_set_auth_cookie( $this->user->wp_id );
                                return view('checkout.subscribed')->with([ 'cookie' => $cookie, 'wlm_content_url' => env("WLM_CONTENT_URL") ]);

                            endif;

                                return view('checkout.subscribed')->with([ 'wlm_content_url' => env("WLM_CONTENT_URL") ]);

                        else:  

                                $request->session()->flash( 'message.level', 'warning');
                                $request->session()->flash( 'message.content', 'Sorry; we could not subscribe you to the plan; please try again later' );                                      

                            return redirect()->back();

                        endif;                    

                }
                
                
                
                private function subscribeUser( Request $request ){

                        if( ( $request->plan == 'trialmonthly' ) ):

                            $this->trialLength = 7;

                            $this->user->newSubscription( $request->plan, $this->plan->braintree_plan )
                                 ->trialDays( $this->trialLength )
                                 ->create( $request->payment_method_nonce, [
                                'email'  => $request->email
                            ]);                      

                            return $this->user->charge(7);

                        elseif( ( $request->plan == 'six-month-savings' ) ):

                            $now                    = Carbon::now();
                            $end                    = Carbon::now()->addMonths(6);
                            $this->trialLength      = $end->diffInDays( $now );

                            $this->user->newSubscription( $request->plan, $this->plan->braintree_plan )
                                 ->trialDays( $this->trialLength )
                                 ->create( $request->payment_method_nonce, [
                                'email'  => $request->email
                            ]);  
                            
                        //plan switch    
                        $trialmonthlyPlan = Plan::where('slug', 'trialmonthly')->first();   
                        if( $this->user->subscribedToPlan( $trialmonthlyPlan->braintree_plan, 'trialmonthly' ) ):   

                            $this->user->subscription( 'trialmonthly' )->cancel();

                        endif;                             

                            return $this->user->charge( 97 );

                        endif; 

                        return false;
                    
                }                
                
                
                
                

                

}

