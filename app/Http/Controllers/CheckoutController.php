<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\WpCookies;
use App\Libraries\WpUser;
use App\Models\User;
use Auth;

class CheckoutController extends Controller
{
    public function index($plan, WpCookies $cookie_obj)
    {
        
       $cookie      = $cookie_obj->wp_parse_auth_cookie();

       if( $wp_user_obj = $cookie_obj->wp_validate_auth_cookie($cookie) ){
           
           if( $laravel_user = User::where( 'email', $wp_user_obj->data['user_email'] ) -> first() ):
               Auth::login($laravel_user);
           endif;
           
           return view('checkout.dropui')->with( ['plan' => $plan, 'wp_user' => $wp_user_obj->data, 'wp_siteurl' =>env("WP_SITEURL"), 'wlm_content_url' => env("WLM_CONTENT_URL") ] ); 
           
       }

           return view('checkout.dropui')->with( [ 'plan' => $plan, 'wp_siteurl' =>env("WP_SITEURL"), 'wlm_content_url' => env("WLM_CONTENT_URL") ]); 
        
    }
}
