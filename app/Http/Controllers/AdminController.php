<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plan;

class AdminController extends Controller
{
    public function index()
    {
        
        
        return view('admin.index')->with(['plans' => Plan::get()]);
    }
}
