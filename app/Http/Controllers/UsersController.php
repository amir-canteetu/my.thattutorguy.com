<?php

namespace App\Http\Controllers;



use App\Libraries\WpPasswordChecker;
use App\Libraries\WpUser;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;

class UsersController extends Controller
{


                
                public function __construct(  ) 
                {
                }

                 /**
                 * Get subscribing user.
                 *
                 * @param  obj      Illuminate\Http\Request
                 * @return obj      App\Models\User
                 */          
                public static function getUser( \Illuminate\Http\Request $request, $has_wp_logged_in_cookie = false )
                {

                    $user       = Auth::user();
                    
                    if( empty( $user ) ):

                            $user  = User::where( 'email', $request->email ) -> first();
                    
                            if( !empty( $user ) ):

                                    $credentials = $request->only( 'email', 'password' );

                                    if ( !Auth::attempt($credentials) ):
                                        
                                            # If Laravel auth attempt fails, given passwrd is either 
                                            # 1. plain-text wp-pass or 
                                            # 2. wp-passwrd hash from cookie   
                                            # if case 1: wp_check_password and return user. 
                                            # If 2. confirm cookie and return user.                                  
		                                                           
                                            if( WpPasswordChecker::wp_check_password( trim($request->password), $user->password ) || $has_wp_logged_in_cookie ):
                                        
                                                return $user;
                                            
                                            else:
                                                
                                                $request->session()->flash( 'message.level', 'warning' );
                                                $request->session()->flash( 'message.content', 'A user with the provided email address already exists in our records, '
                                                . 'but it seems you have forgotten your password. Please try again or reset your password <a href="https://www.thattutorguy.com/wp-login.php?action=lostpassword">here</a>' );                                             

                                                return false;
                                            
                                            endif;

                                    endif;

                            else: # User does not exist in Laravel db; let's check in wp; 
                                  # If not in wp, create new Laravel user
                                    $wp_user_obj  = WpUser::checkForWpUser($request);
                            
                                    if( empty( $wp_user_obj ) ): 

                                            return self::createLaravelUser($request, $has_wp_logged_in_cookie); 

                                    else:   # If user does not exist in Laravel but does exist in wp there are 2 cases
                                            # 1. if no cookie, given passwrd is plain text. hash and compare.
                                            # 2. if cookie; use cookie auth. 

                                            if( WpPasswordChecker::wp_check_password( trim($request->password), $wp_user_obj->user_pass, $wp_user_obj->ID ) || $has_wp_logged_in_cookie ):

                                                return  self::createLaravelUser( $request, $has_wp_logged_in_cookie, $wp_user_obj );                                           

                                            else: # Password mismatch 

                                                $request->session()->flash( 'message.level', 'warning' );
                                                $request->session()->flash( 'message.content', 'A user with the provided email address already exists in our records, '
                                                . 'but it seems you have forgotten your password. Please try again or reset your password <a href="https://www.thattutorguy.com/wp-login.php?action=lostpassword">here</a>' );                                            

                                                return false;
                                                
                                            endif; 

                                        
                                    endif;

                            endif;

                    endif;
                    
                    return $user;
          

                }


                
                 /**
                 * Create new user in Laravel.
                 *
                 * @param  obj      Illuminate\Http\Request
                 * @return obj      App\Models\User
                 */                   
                public static function createLaravelUser( Request $request, $has_wp_logged_in_cookie = false, $wp_user_obj = null ) 
                {

                        $user           = new User();
                        
                        if($has_wp_logged_in_cookie):
                            $user->password = trim( $request->password );
                            else:
                            $user->password = Hash::make( trim( $request->password ) );
                        endif;
                        
                        $user->email         = trim( $request->email );
                        $user->name          = trim( $request->name );
                        $user->zip           = !empty(trim( $request->zip )) ? trim( $request->zip ) : null;
                        
                        if( is_object( $wp_user_obj ) ):
                            $user->wp_id = $wp_user_obj->ID;
                        endif;

                        $user->save();
                        Auth::login($user);
                        
                        return $user;

                }

}
