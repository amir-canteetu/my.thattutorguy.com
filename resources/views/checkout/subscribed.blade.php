@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Redirecting to Thattutorguy</div>

                <div class="card-body">
                    Thank you for subscribing. You will be redirected to Thattutorguy in a moment.
                </div>
            </div>
        </div>
    </div>
</div>
 
 
                @if(isset($cookie))

<script>

                    (function($) {
                        document.cookie =  '{{ $cookie['name'] }}={{ $cookie['value'] }};expires={{ $cookie['expire'] }};path={{ $cookie['path'] }};domain={{ $cookie['domain'] }}';
                    })(jQuery); 

</script>           

                @endif


<script>


    (function($) {
        
            setTimeout(function(){
                
                window.location = "{{ $wlm_content_url }}";
                
            }, 5000);
        
    })(jQuery); 

</script>

@endsection

