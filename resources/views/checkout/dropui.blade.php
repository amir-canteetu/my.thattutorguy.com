@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6">
	      
            @if ( !isset($wp_user))

                <div class="confirm_subscribed">
                    <p>Are you a returning user? Please <a href="{{ $wp_siteurl }}/wp-login.php"><button type="button" class="btn btn-primary wp_siteurl">login here</button></a> first so that you'll have access to all your playlists, play history, etc.</p>
                </div>             

            @endif						
  
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                @if (isset($wp_user))
                
                <h3>Hi {{ isset( $wp_user['display_name'] ) ? $wp_user['display_name'] : $wp_user['user_login'] }}, just fill in your payment info below for your new subscription.</h3>
                    
                @endif
            
            <form action="{{ route('subscribe') }}" method="post">
                
                {{ csrf_field() }}
              
                    <input type="hidden" name="plan" value="{{ $plan }}">
              
                @if (isset($wp_user))
                
                    <input type="hidden" name="email" class="form-control" value="{{ $wp_user['user_email'] }}">
                    <input type="hidden" name="password" class="form-control" value="{{ $wp_user['user_pass'] }}">
                    <input type="hidden" name="name" class="form-control" value="{{ $wp_user['user_nicename'] }}">
                    
                @else
                
                <div class="d-none">
                    
                    <div class="form-group">
                      <p>Provide your details</p>  
                      <label for="InputEmail1">Email address</label>
                      <input type="email" name="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}" required>
                    </div>
                    
                    <div class="form-group">
                      <label for="InputName">Name</label>
                      <input type="text" name="name" class="form-control" id="InputName" placeholder="Enter name" value="{{ old('name') }}" required>
                    </div>

                    <div class="form-group">
                      <label for="InputName">Zip</label>
                      <input type="text" name="zip" class="form-control" placeholder="Enter zip" value="{{ old('zip') }}">
                    </div>                    
              
                    <div class="form-group">
                      <label for="InputPassword">Password</label>
                      <input id="password" type="password" name="password" class="form-control" id="InputPassword" placeholder="Password" required>
                      <span id="emsg" style="display: none; color: red;">The password needs to have at least six characters</span>
                    </div>                     
                </div>
                
                @endif               

            <div class="panel panel-default"> 
                
                <div class="subscription_info d-none"> 
                    
                @if ($plan === 'trialmonthly')

                    <p>You're subscribing to the Trial/Monthly Plan</p>

                    <p>Subscriptions → 7 Days for $7 Trial , $30 Monthly thereafter</p>

                    <p>Today you will be billed: $7</p>

                    <p>After payment you will have immediate access to all our videos.</p>
                    
                    <p>Would you rather like to subscribe to the <button type="button" class="btn btn-primary"><a href="{{ url('/checkout/six-month-savings') }}">savings plan?</a></button></p>

                </div><!-- .subscription_info -->   
                <div class="panel-body">            

                @elseif ($plan === 'six-month-savings')
                
                    <p>You're subscribing to the 6-Month Savings Plan</p>

                    <p>Subscriptions → 6 months for $97, followed by $16/month</p>

                    <p>Today you will be billed: $97</p>

                    <p>After payment you will have immediate access to all our videos.</p>
                    
                    <p>Would you rather like to subscribe to the <button type="button" class="btn btn-primary"><a href="{{ url('/checkout/trialmonthly') }}">trial/monthly plan?</a></button></p>

                </div><!-- .subscription_info -->   
                <div class="panel-body">            

                @endif                 
              
                    <div id="dropin-container"></div>              

                    <button id="payment-button" class="btn btn-primary btn-flat d-none" type="submit">Pay now</button>
              
                    </div><!-- .panel-body -->    
                  </div><!-- .panel panel-default -->    
              
            </form>          
              
         </div><!-- col-md-8 -->   
          
     </div><!-- .row -->           
  </div><!-- .container -->
  
    @if (isset($wp_user))
    
    <script>
    (function($) {        
        
        $.ajax({
            url: '{{ url('braintree/token') }}'
        }).done(function (response) {
            
            braintree.setup(response.data.token, 'dropin', {
                container: 'dropin-container'
            });
            $('#payment-button, .form-group, .subscription_info, .hide').removeClass('d-none hide');
        });
        
    })(jQuery); 
    </script>      
    
    @else

    <script>
        
    (function($) {        
        
        $.ajax({
            url: '{{ url('braintree/token') }}'
        }).done(function (response) {
            braintree.setup(response.data.token, 'dropin', {
                container: 'dropin-container',
                onReady: function () {
                    $('#payment-button, .form-group, .subscription_info').removeClass('d-none');
                }
            });
        });
        
    })(jQuery); 
    
    </script>  

    @endif     

    <script>
        
    (function($) {        
        
    var $regexname=/^.{6,}$/;
    $('#password').on('keypress keydown keyup onpaste',function(){
             if (!$(this).val().match($regexname)) {
                 $('#emsg').removeClass('hidden');
                 $('#emsg').show();
                 $(this).addClass('error');
             }
           else{
                $('#emsg').addClass('hidden');
                $(this).removeClass('error');
               }
    });
        
    })(jQuery); 
    
    </script>

@endsection

@section('braintree')
    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>    
@endsection


