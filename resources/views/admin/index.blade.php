@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Plans</div>

                <div class="panel-body">
                    <ul class="list-group">
                        @foreach ($plans as $plan)
                            <li class="list-group-item clearfix">
                                
                                <div class="pull-left">
                                    <h4>{{ $plan->name }}</h4>
                                    @if ($plan->description)
                                    <p><strong>Description:</strong> {{ $plan->description }}</p>
                                    @endif
                                    @if ($plan->braintree_plan)
                                        <p><strong>WLM Level ID:</strong> {{ $plan->braintree_plan }}</p>
                                    @endif 
                                    @if ($plan->slug)
                                        <p><p><strong>Slug:</strong> {{ $plan->slug }}</p>
                                    @endif                                      
                                </div>

                            </li>
                        @endforeach
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Wishlist Member & Wordpress Config</div>

                <div class="panel-body">
                    
                        <form>
                            
                                <div class="form-group">
                                  <label for="wlm_api_key">WLM API KEY</label>
                                  <input type="text" name="wlm_api_key" class="form-control" id="wlm_api_key" placeholder="Enter WLM API KEY">
                                </div>

                                <div class="form-group">
                                  <label for="wlm_api_key">WLM API URL</label>
                                  <input type="text" name="wlm_api_url" class="form-control" id="wlm_api_url" placeholder="Enter WLM API URL">
                                </div>
                            
                            
                                <div class="form-group">
                                  <label for="wlm_api_key">WLM CONTENT URL</label>
                                  <input type="text" name="wlm_content_url" class="form-control" id="wlm_content_url" placeholder="Enter WLM CONTENT URL">
                                </div>                            


                                <div class="form-group">
                                  <label for="db_host_wordpress">WORDPRESS DB HOST</label>
                                  <input type="text" name="db_host_wordpress" class="form-control" id="db_host_wordpress" placeholder="127.0.0.1">
                                </div>

                                <div class="form-group">
                                  <label for="wlm_api_key">WORDPRESS DB PORT</label>
                                  <input type="text" name="db_port_wordpress" class="form-control" id="db_port_wordpress" placeholder="3306">
                                </div>
                            
                                <div class="form-group">
                                  <label for="wlm_api_key">WORDPRESS DB NAME</label>
                                  <input type="text" name="db_database_wordpress" class="form-control" id="db_database_wordpress" placeholder="Enter WORDPRESS DB NAME">
                                </div>

                                <div class="form-group">
                                  <label for="wlm_api_key">WORDPRESS DB USERNAME</label>
                                  <input type="text" name="db_username_wordpress" class="form-control" id="db_username_wordpress" placeholder="Enter WORDPRESS DB USERNAME">
                                </div>

                                <div class="form-group">
                                  <label for="wlm_api_key">WORDPRESS DB PASSWORD</label>
                                  <input type="text" name="db_password_wordpress" class="form-control" id="db_password_wordpress" placeholder="Enter WORDPRESS DB PASSWORD">
                                </div> 
                            
                            

                                <div class="form-group">
                                  <label for="wlm_api_key">WP_LOGGED_IN_KEY</label>
                                  <input type="text" name="wp_logged_in_key" class="form-control" id="wp_logged_in_key" placeholder="Enter WP_LOGGED_IN_KEY">
                                </div>  

                                <div class="form-group">
                                  <label for="wlm_api_key">WP_LOGGED_IN_SALT</label>
                                  <input type="text" name="wp_logged_in_salt" class="form-control" id="wp_logged_in_salt" placeholder="Enter WP_LOGGED_IN_SALT">
                                </div>  

                                <div class="form-group">
                                  <label for="wlm_api_key">WP_COOKIE_DOMAIN</label>
                                  <input type="text" name="wp_cookie_domain" class="form-control" id="wp_cookie_domain" placeholder="Enter WP_COOKIE_DOMAIN">
                                </div>  
                            
                            
                                <button type="submit" class="btn btn-primary">Submit</button>
                          
                          
                        </form>
                    
                    
                    
                    
                </div>
                
            </div>
        </div>
    </div>    
    
</div>
@endsection