<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/checkout/{plan}', 'CheckoutController@index');

Route::get('/braintree/token', 'BraintreeTokenController@token');

Route::post('/subscribe', 'SubscriptionsController@store')->name('subscribe');
 
Route::get('/subscribed', function () {
    return view('checkout.subscribed');
});


Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/admin', 'AdminController@index');
    
    Route::get('/users', 'UsersController@show');
    Route::get('/plans', 'PlansController@index');    
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

});


